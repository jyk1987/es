# es

#### 介绍
easy service
一个应用于go语言的服务框架，宗旨在于使用简单，部署简单，便于组织不同结构类型。
项目目前处于开发阶段

#### 实现理念
抛弃以往微服务架构中的比较重的服务治理框架，使用去中心化的架构来实现。
初步想法是整体系统中分为三个角色

1. IS，索引服务，用于不同服务之前互相发现
   
2. NODE，节点，就是不同的业务服务节点
   
3. WOKER，工作者，提供第三方服务，比如列队、流量消峰、定时执行｜延迟执行等不同形式的服务。
#### 实现计划
1. 测试并选择通讯方式
   
2. 确定服务暴露形式和服务调用形式

3. 实现IS，使用http方式进行交互

4. 增加数据加密和关注安全问题

5. 实现负载均衡和坏点剔除
   
实现以上后发布1.0版本就可以投入使用了，2.0版本对服务的不同注册和调用机制进行增强并实现工作者角色。

#### 使用
1. go版本需要>=1.17

2. 目前版本依赖rpcx做底层通讯所有需要先安装 go get -u github.com/smallnest/rpcx/...